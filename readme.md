# Copy environment script


**!!! ATTENTION !!! This repo si public! - Do not include any SSH keys or sensitive information.**


This script is used to clone factorify from production into test environment. It:

* Automatically stops and starts factorify and planning docker services
* Clones DB
* Sync storage (storage, unsecure_storage, ... [everything inside directory ```local_fy_root```])

Do not modify ```copy-environment.sh```! It is supposed to be modified in the future. Change only configuration 
file ```env.conf```. Copy ```env-example.conf``` when you are doing fresh install.

## Install
* Clone or copy this repo into production (probably /home/factorify)
* Generate new ssh key pair ```ssh-keygen```
	* Place private key in some secure location in the production environment. Configure the property ```remote_ssh_private_key``` in ```env.conf```.
	* Place public key in ```/home/<user>/.ssh/authorized_keys``` in the test environment
* Ensure ```remote_ssh_user``` can run commands as root without asking for password
	* ```sudo visudo```
	* Add following at the file end: ```<user>   ALL=(ALL:ALL) ALL```

## Usage
Be sure you have properly:
* Filled configuration into env.conf
* Copied required private keys used for authentication to test env via ssh

Run:

```./copy-environment.sh```