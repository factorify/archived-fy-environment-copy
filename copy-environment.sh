#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# >>>> Start of configuration variable definitions
start_stop_docker_services=
# Local
local_fy_root=
local_db_host=
local_db_database=
local_db_username=
local_db_password=
# Remote
remote_ssh_user=
remote_ssh_domain=
remote_ssh_private_key=
remote_db_host=
remote_db_database=
remote_db_username=
remote_db_username_ro=
remote_db_password=
remote_fy_root=
# <<<<< End of configuration variable definitions

# Load variable from configuration file
source env.conf

# Common
tmp_dir=/tmp/fy-environment-copy/
db_dump_filename=""
db_dump_path=""
docker_factorify_container=factorify_factorify_1
docker_planning_container=factorify_planning_1


# Functions
log() {
    local current_time=`date "+%Y-%m-%d  %H:%M:%S"`
    echo "$current_time [INFO] - $1"
}

remote_ssh_connection_string() {
    echo "${remote_ssh_user}@${remote_ssh_domain}"
}

run_ssh_cmd() {
    ssh -i ${remote_ssh_private_key} $(remote_ssh_connection_string) $1
}

# $1 - line
# $2 - file
append_line_to_file() {
    echo $1 >> $2
}

dump_db() {
    log "Start dumping DB"

    db_dump_filename=fy_db_dump-$(date +"%Y-%m-%d_%H-%M").sql
    db_dump_path="$tmp_dir${db_dump_filename}"

    cat << DROPSCHEMA > ${db_dump_path}
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
ALTER SCHEMA public OWNER TO ${remote_db_username};

DROP SCHEMA planning_output CASCADE;
CREATE SCHEMA planning_output AUTHORIZATION ${remote_db_username};

GRANT USAGE ON SCHEMA public TO ${remote_db_username_ro};
GRANT USAGE ON SCHEMA planning_output TO ${remote_db_username_ro};

ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO ${remote_db_username_ro};
ALTER DEFAULT PRIVILEGES IN SCHEMA planning_output GRANT SELECT ON TABLES TO ${remote_db_username_ro};

GRANT SELECT ON ALL TABLES IN SCHEMA public TO ${remote_db_username_ro};
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO ${remote_db_username_ro};
GRANT SELECT ON ALL TABLES IN SCHEMA planning_output TO ${remote_db_username_ro};
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA planning_output TO ${remote_db_username_ro};
DROPSCHEMA

    export PGPASSWORD=${local_db_password}
    pg_dump -O --no-privileges -h ${local_db_host} -U ${local_db_username} -s -n planning_output >> ${db_dump_path}
    pg_dump -O --no-privileges -h ${local_db_host} -U ${local_db_username} -n public --exclude-table-data=versions --exclude-table-data=solution_annealing_furnaces --exclude-table-data=artificial_aging_furnaces --exclude-table-data=temperatures --exclude-table-data=workflow_cache >> ${db_dump_path}

    log "Dumping done into: ${db_dump_path}"
}

copy_local_to_remote() {
    echo "COPY from (local): $1 to (remote): $(remote_ssh_connection_string):$2"
    rsync -av --copy-links --rsync-path="sudo rsync" -e "ssh -i ${remote_ssh_private_key}" $1 "$(remote_ssh_connection_string):$2"
}

copy_storage() {
    log "Start copying storage"
    copy_local_to_remote ${local_fy_root} ${remote_fy_root}
    log "Copying done"
}

# $1 - local sql script file
run_sql_script() {
    log "Running sql in file: $1"

    local base_sql_name=$(basename $1)
    local remote_sql_path="$tmp_dir$base_sql_name"

    copy_local_to_remote ${1} ${remote_sql_path}
    run_ssh_cmd "export PGPASSWORD='$remote_db_password'; psql -h $remote_db_host -U $remote_db_username -d $remote_db_database < ${remote_sql_path}"
    log "SQL file run finished"
}

migrate_db_to_remote() {
    log "Start migrating DB"
    run_sql_script ${db_dump_path}

    log "Running POST migration SQL scripts:"
    for file in ${SCRIPT_DIR}/post-migration-sql-scripts/*; do
        run_sql_script ${file}
    done
    log "DB migration completed"
}

start_docker_services() {
    if [[ ${start_stop_docker_services} == true ]]; then
        log "Starting factorify"
        run_ssh_cmd "sudo docker start ${docker_factorify_container}"
        log "Waiting for 2 minutes to let run factorify increments before starting planning"
        sleep 2m
        log "Starting planning"
        run_ssh_cmd "sudo docker start ${docker_planning_container}"
    else
        log "Omitting factorify docker services start"
    fi
}

stop_docker_services() {
    if [[ ${start_stop_docker_services} == true ]]; then
        run_ssh_cmd "sudo docker stop ${docker_factorify_container} ${docker_planning_container}"
    else
        log "Omitting factorify docker services stop"
    fi
}

init() {
    mkdir ${tmp_dir}
    run_ssh_cmd "mkdir $tmp_dir"
}

cleanup() {
    log "Cleanup"
    rm -r ${tmp_dir}
    run_ssh_cmd "rm -r $tmp_dir"
    log "Cleanup finished"
}

# Script part

log "Starting script"
init
stop_docker_services

dump_db
migrate_db_to_remote
copy_storage
cleanup
start_docker_services
log "Migrating environment completed"