UPDATE authentication_details
SET password_hash = '$2a$10$vcFr/Kzm6T93StN7ojxGgexi5LvA2q/f91FIQUIuf9lbmUdUsuIRG'
WHERE person_id = 1;